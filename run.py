#!/usr/bin/env python

"""
Call functions used to generate a series of derivatives from input bathymetry raster.
"""

__author__ = "Cole Fields"
__credits__ = "Cole Fields"
__maintainer__ = "Cole Fields"
__email__ = "Cole.Fields@dfo-mpo.gc.ca"
__status__ = "Development"

# Import modules
import os
import time
import getpass
import socket
import datetime as dt
import arcpy
import env_data_functions as edf
import logging

# Set overwrite to True
arcpy.env.overwriteOutput = True


def main():
    # Start execution time.
    start = time.time()

    # String break message between functions.
    break_msg = "\n------------------------------------------------------------------------------------------\n"

    # Messages prompting user for input. These are used if user does not provide command line arguments.
    invalid_input_msg = "Invalid input.\n"
    flip_prompt = "Enter True or False to flip bathymetry values prior to calculating derivatives: (if depth values " \
                  "are positive, type True, if depth values are negative, print False.\n"
    bathy_prompt = "Enter a path to bathymetry layer (GeoTIFF or Geodatabase raster): \n"
    smooth_prompt = "Enter True or False to smooth bathymetry prior to calculating rugosity, slope, and curvature: \n"
    neighbourhood_prompt = "Enter a positive integer representing the number of cells to be used as the radius for" \
                           " circular focal mean (smoothing bathymetry): \n"
    out_path_prompt = "Enter an existing output filepath or a filepath with new directory to be created: \n"

    # Prompt the user to provide arguments.
    # Check if the input is a tif file or that it exists.
    input_bathymetry = edf.get_bathy_path(bathy_prompt, invalid_input_msg)
    # Should bathy be flipped?
    flip_bathy = edf.get_bool(flip_prompt, invalid_input_msg, flip_prompt)
    # Prompt user until they provide either "True" or "False".
    smooth_bathymetry = edf.get_bool(smooth_prompt, invalid_input_msg, smooth_prompt)
    # If user entered True for smoothing bathymetry, prompt for positive integer to smooth by.
    if smooth_bathymetry is True:
        # Prompt user until the provide a positive integer greater than zero.
        smooth_value = edf.get_positive_int(neighbourhood_prompt, invalid_input_msg)
    else:
        smooth_value = None

    # Create or change to output directory.
    output_directory = edf.change_mk_dir(out_path_prompt)

    # Write a text file in output directory.
    logging.basicConfig(filename=os.path.join(output_directory, "messages-derivatives.log"),
                        filemode="w",
                        format="%(message)s",
                        level=logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    logging.getLogger("").addHandler(console)

    logging.info(break_msg)
    logging.info("Script run on {} by {} on {}".format(dt.date.today(),
                                                               getpass.getuser(),
                                                               socket.gethostname()))
    logging.info(break_msg)

    # Copy input raster to output directory.
    bathymetry_lyr = edf.copy_raster(input_bathymetry)

    logging.info(break_msg)

    # If user entered True for smoothing bathymetry, prompt for positive integer to smooth by.
    if flip_bathy is True:
        # Prompt user until the provide a positive integer greater than zero.
        bathymetry_lyr = edf.sign_flip(bathymetry_lyr)
        logging.info(break_msg)

    # Calculate derivatives.
    edf.calc_derivatives(bathymetry_lyr, break_msg, output_directory, smooth_bathymetry, smooth_value)

    # Set extent for raster layers.
    edf.set_extent(output_directory)

    # End run time.
    end = time.time()
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)

    logging.info(break_msg)

    # Write processing time.
    logging.info("Processing time:\n{:0>2}:{:0>2}:{:05.2f}\n".format(int(hours), int(minutes), seconds))
    logging.info(break_msg)

    # Add additional notes about processing.
    if flip_bathy:
        logging.info("bathymetry_flipped.tif is the bathymetry layer with negative depth values.")
    if smooth_value is None:
        logging.info("Input bathymetry was not smoothed prior to creating any derivatives.")
    else:
        logging.info("Input bathymetry was smoothed by a {} cell radius focal mean using a "
                     "circular neighbourhood.".format(smooth_value))
        logging.info("The smoothed bathymetry was used to generate slope -> standard deviation of slope, curvature, "
                     "rugosity, aspect, and hillshade.")
        logging.info("The bathymetry.tif layer in the output directory is not smoothed.")
    logging.info(break_msg)


if __name__ == "__main__":
    main()
