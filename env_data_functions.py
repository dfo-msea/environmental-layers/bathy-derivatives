#!/usr/bin/env python

"""
Functions used to generate a series of derivatives from input bathymetry raster. Several scripts taken from BTM
toolbox and slightly modified. See Acknowledgements for details.
"""

__author__ = "Cole Fields"
__credits__ = ["Cole Fields", "Wright, D.J., Pendleton, M., Boulware, J., Walbridge, S., Gerlt, B., Eslinger, "
                              "D., Sampson, D., and Huntley, E. 2012. ArcGIS Benthic Terrain Modeler (BTM), "
                              "v. 3.0, Environmental Systems Research Institute, NOAA Coastal Services Center, "
                              "Massachusetts Office of Coastal Zone Management. "
                              "Available online at http://esriurl.com/5754."]
__maintainer__ = "Cole Fields"
__email__ = "Cole.Fields@dfo-mpo.gc.ca"
__status__ = "Development"


# Import modules
import os
import arcpy
from arcpy.sa import *
import surface_area_to_planar_area as acr
import bpi
import utils
import logging
import rasterio as rio # for reading and writing rasters and spatial indexing of rasters
import numpy as np # for array math
from scipy import ndimage as ndi  # for focal calculations
import traceback

# Check out Spatial Analyst extension from ESRI.
arcpy.CheckOutExtension("Spatial")


def get_bool(prompt, invalid_msg, smooth_msg):
    """Prompt user for True/False string and convert to Boolean type.

    :param prompt: String prompt for user
    :param invalid_msg: Message indicating invalid input from user
    :param smooth_msg: True or False
    :return: boolean value
    """

    while True:
        try:
            return {"true": True, "false": False}[input(prompt).lower()]
        except KeyError:
            print(invalid_msg, smooth_msg)


def get_bathy_path(prompt, invalid_msg):
    """Return the string from user input. This prompts user until they enter a correct file path. Checks that the path
    ends with ".tif" or that it exists with arcpy.Exists (which is needed to check in geodatabases).

    :param prompt: String prompt for user
    :param invalid_msg: Message indicating invalid input from user
    :return: Valid filepath to existing dataset, name of raster without extension or filepath
    """

    while True:
        try:
            path = os.path.normpath(input(prompt))
            if path.endswith(".tif") or arcpy.Exists(path):
                return path
        except KeyError:
            print(invalid_msg, prompt)


def change_mk_dir(dir_prompt):
    """Make an output directory.
    :param dir_path: Filepath to existing directory or filepath with directory to create.
    :return: dir_path
    """
    dir_path = os.path.normpath(input(dir_prompt))

    # Check if output directory already exists, if not, create it.
    if not os.path.exists(dir_path):
        print("Creating output directory here: {}".format(dir_path))
        os.mkdir(dir_path)
        print("Changing directory to {}.".format(dir_path))
        os.chdir(dir_path)
        return dir_path
    else:
        print("Changing directory to {}.".format(dir_path))
        os.chdir(dir_path)
        return dir_path


def make_raster_set_env(raster_file):
    """Set the arc environment settings (snap) and get create raster layer from input raster file.

    :param raster_file: Validated path to raster file
    :return: Raster layer
    """

    # Set snap environment to input raster.
    arcpy.env.snapRaster = raster_file
    # Create a raster layer from input file.
    raster_layer = arcpy.sa.Raster(raster_file)
    return raster_layer


def get_cell_size(raster_layer):
    """Get the cell size in source units.

    :param raster_layer: Raster layer from arcpy.sa.Raster
    :return: Cell size of raster in units of coordinate system
    """

    # Get raster cell size from the x-direction - Assumes a regular-sized grid (same cell size in x and y direction).
    logging.info("Retrieving cell size from {}".format(raster_layer))
    cell_size = int(arcpy.GetRasterProperties_management(raster_layer, "CELLSIZEX").getOutput(0))
    logging.info("Cell size of raster is {}".format(cell_size))
    return cell_size


def copy_raster(raster_layer):
    """Create a copy of the input raster. Exports a GeoTiff file to output directory in 32 bit float format.

    :param raster_layer: Original input bathymetry layer
    :return: path to raster
    """

    logging.info("Copying {} to {}".format(raster_layer, os.getcwd()))
    out_name = os.path.join(os.getcwd(), "bathymetry" + '.tif')
    arcpy.CopyRaster_management(raster_layer, out_name, pixel_type="32_BIT_FLOAT")
    return out_name


def sign_flip(copied_raster):
    """
    :param copied_raster: raster layer
    return: sign-flipped raster
    """
    out_name = os.path.join(os.getcwd(), "bathymetry_flipped" + '.tif')
    logging.info("Changing positive depth values from {} to negative and exporting as {}...\n"
                 "Derivatives will be derived from this bathymetry or a smoothed version "
                 "of it.".format(copied_raster, out_name))
    raster_layer = arcpy.sa.Raster(copied_raster)
    flipped_raster = raster_layer * -1
    arcpy.CopyRaster_management(flipped_raster, out_name, pixel_type="32_BIT_FLOAT")
    return out_name


def get_positive_int(integer_msg, invalid_msg):
    """Get a positive integer from user that represents the number of cells to use as the focal mean neighbourhood.

    :param integer_msg:  Message to user to provide a positive integer
    :param invalid_msg:  Message to user that input is invalid
    :return:
    """

    while True:
        try:
            value = int(input(integer_msg))
            # Test if prompt is a number.
            if value > 0:
                # Convert to integer.
                return int(value)
            else:
                print("{} is not a positive integer.".format(value))
            # Try to convert it to an integer.
        except ValueError:
            # Message to users.
            print(invalid_msg, integer_msg)


def smooth_raster(raster_layer, smoothing_value, brk_msg):
    """Runs a focal mean on input bathymetry raster to smooth the layer.

    :param raster_layer: Input bathymetry layer
    :param smoothing_value: Positive integer (neighbourhood for focal mean calculation)
    :param brk_msg: String to break up print messages
    :return: In-memory layer of smoothed bathymetry to be used as input for generating some derivatives
    """

    try:
        logging.info(brk_msg + "Smoothing {} by {} cells...".format(raster_layer, smoothing_value) + brk_msg)
        out_smooth = FocalStatistics(raster_layer, NbrCircle(smoothing_value, "Cell"), "MEAN", "DATA")
        return out_smooth
    except Exception:
        logging.error(traceback.format_exc())


def standardize_bpi(bpi_raster=None, out_raster=None):
    """Standardizes BPI layers using tools in the BTM scripts.

    :param bpi_raster: BPI raster
    :param out_raster: Standardized BPI layer
    :return:
    """
    try:
        arcpy.env.compression = "LZW"
        # Get raster properties.
        message = ("Calculating properties of the Bathymetric "
                   "Position Index (BPI) raster...")
        logging.info(message)
        logging.info("  input raster: {}\n   output: {}".format(bpi_raster, out_raster))
        # Convert to a path.
        desc = arcpy.Describe(bpi_raster)
        bpi_raster_path = desc.catalogPath

        bpi_mean = utils.raster_properties(bpi_raster_path, "MEAN")
        logging.info("BPI raster mean: {}.".format(bpi_mean))
        bpi_std_dev = utils.raster_properties(bpi_raster_path, "STD")
        logging.info("BPI raster standard deviation: {}.".format(bpi_std_dev))

        # Create the standardized Bathymetric Position Index (BPI) raster.
        std_msg = "Standardizing the Bathymetric Position Index (BPI) raster..."
        logging.info(std_msg)
        arcpy.env.rasterStatistics = "STATISTICS"
        out_bpi = Int(Plus(Times(Divide(Minus(bpi_raster_path, bpi_mean), bpi_std_dev), 100), 0.5))
        arcpy.CopyRaster_management(out_bpi, out_raster)
        success_msg = "Standardized bpi has replaced original...\n"
        arcpy.Delete_management(bpi_raster)
        logging.info(success_msg)
    except Exception:
        logging.error(traceback.format_exc())
    return


def calc_slope(raster_layer):
    """Calculate slope in degrees from input raster.

    :param raster_layer: Input raster layer
    :return: In-memory slope layer
    """

    try:
        out_slope = Slope(in_raster=raster_layer, output_measurement="DEGREE")
        return out_slope
    except Exception:
        logging.error(traceback.format_exc())


def calc_aspect(raster_layer):
    """Calculate aspect from input raster.

    :param raster_layer: Input raster layer
    :return: In-memory Aspect
    """

    try:
        out_aspect = Aspect(in_raster=raster_layer)
        return out_aspect
    except Exception:
        logging.error(traceback.format_exc())


def calc_hillshade(raster_layer):
    """Calculate hillshade from input raster.

    :param raster_layer: Input raster layer
    :return: In-memory hillshade
    """

    try:
        # Create hillshade layer
        out_hillshade = Hillshade(in_raster=raster_layer)
        return out_hillshade

    except Exception:
        logging.error(traceback.format_exc())


def calc_sd_slope(slope_lyr, window):
    """Calculate standard deviation of slope from input raster.

    :param slope_lyr: Slope layer
    :param slope_lyr: Number for window to use for calculating std dev of layer
    :return: In-memory standard deviation of slope
    """

    try:
        # Open bathy raster temporarily
        with rio.open(slope_lyr) as ras:
            # Get profile for output file
            profile = ras.profile
            # Get as np array
            array = ras.read(1)
            # Convert to float
            array = array * 1.
            # Convert no data to nan
            nodata = ras.nodatavals
            array[array == nodata] = 'nan'
            # Calculate standard deviation
            array_sd = ndi.generic_filter(array, np.std, size=window)

            # Output raster file name
            out_dir = os.path.dirname(str(slope_lyr))
            output_sd = os.path.join(out_dir, "standard_deviation_slope.tif")

            # Write masked point density array
            with rio.Env():
                # Update profile
                profile.update(
                    dtype=rio.float32,
                    count=1,
                    compress='lzw')
                # Write raster
                with rio.open(output_sd, 'w', **profile) as dataset:
                    dataset.write(array_sd.astype(rio.float32), 1)

        return output_sd
    except Exception:
        logging.error(traceback.format_exc())


def calc_curvature(raster_lyr):
    """Calculate curvature from input raster.

    :param raster_lyr: Input raster layer
    :return: In-memory curvature layer
    """

    try:
        out_curv = Curvature(raster_lyr)
        return out_curv
    except Exception:
        logging.error(traceback.format_exc())


def calc_rug(raster_lyr, output_directory_path):
    """Calculate arc-chord ratio rugosity from input raster.

    :param raster_lyr: Input raster layer
    :param output_directory_path: Filepath for output directory
    :return:
    """

    try:
        logging.info("\nCalculating rugosity using Surface Area to Planar Area in BTM scripts. The ratio of surface area "
              "to planar area to corrected planar area is also known as arc-chord ratio rugosity.")
        logging.info("\nCreating a temporary file geodatabase for processing...")

        # Create temp file gdb for processing and set workspace and scratch space.
        tmp_path = os.path.join(output_directory_path, "temp.gdb")
        temp_gdb = arcpy.CreateFileGDB_management(output_directory_path, "temp.gdb")
        arcpy.env.scratchWorkspace = tmp_path
        arcpy.env.workspace = tmp_path
        output_raster = "rugosity"
        out_tif = os.path.join(output_directory_path, "rugosity.tif")

        # Calculate rugosity (tool will only export to fGDB).
        acr.surface_rugosity(tmp_path, in_raster=raster_lyr, out_raster=output_raster, acr_correction=True)
        # Copy raster from fGDB to tif and delete fGDB.
        logging.info("Converting raster grid to GeoTIFF format and deleting temporary file geodatabase...")
        arcpy.CopyRaster_management(output_raster, out_tif)
        arcpy.Delete_management(temp_gdb)
    except Exception:
        logging.error(traceback.format_exc())


def calc_bpi(raster_layer, scale, inner_radius, outer_radius, output_directory_path):
    """Calculate bathymetric position index (BPI) from input raster.

    :param raster_layer: Input raster layer
    :param scale: Text scale value ("broad", "medium", or "fine")
    :param inner_radius: Inner radius used for annulus
    :param outer_radius: Outer radius used for annulus
    :param output_directory_path: Filepath for output directory
    :return: Name of BPI layer
    """

    try:
        # Calculate bpi.
        logging.info("Calculating {} BPI from {}...".format(scale, raster_layer))
        inner_rad = inner_radius
        out_rad = outer_radius
        logging.info("Inner Radius: {}\nOuter Radius: {}\n".format(inner_rad, out_rad))
        bpi_name = os.path.join(output_directory_path, "bpi_{}_temp.tif".format(scale))
        bpi_lyr = bpi.main(bathy=raster_layer,
                           inner_radius=inner_rad,
                           outer_radius=out_rad,
                           out_raster=os.path.join(output_directory_path, "bpi_{}_temp.tif".format(scale)),
                           bpi_type=scale)
        return bpi_name
    except Exception:
        logging.error(traceback.format_exc())


def calc_derivatives(raster_layer, brk_msg, output_directory_path, smooth_raster_lyr, smooth_raster_value):
    """Calls a series of functions to calculate derivatives from input bathymetry.

    :param raster_layer: Input raster layer
    :param brk_msg: String for a break message
    :param output_directory_path: Filepath for output directory
    :param smooth_raster_lyr: True/False to smooth input raster layer
    :param smooth_raster_value: If smooth_raster_lyr is True, the cell value to smooth raster by
    :return:
    """

    try:
        # If the user has specified to smooth the bathymetry layer, smooth prior to creating rugosity, slope,
        # curvature and standard deviation of slope. Do not use smoothed bathymetry for BPI calculations.
        if smooth_raster_lyr is True:
            # Smooth bathymetry by specified number of cells.
            smoothed_raster = smooth_raster(raster_layer, smooth_raster_value, brk_msg)
            smoothed_raster.save(os.path.join(output_directory_path, "bathymetry_smoothed.tif"))

            # Calculate slope and save to disk.
            logging.info(brk_msg + "Calculating Slope from {}...".format(smoothed_raster) + brk_msg)
            out_slope = calc_slope(smoothed_raster)
            out_slope_path = os.path.join(output_directory_path, "slope.tif")
            out_slope.save(out_slope_path)

            # Calculate standard deviation of slope and save to disk.
            logging.info(brk_msg + "Calculating Standard Deviation of Slope from {}...".format(out_slope_path) + brk_msg)
            out_sd_slope = calc_sd_slope(out_slope_path, 3)

            # Calculate aspect.
            logging.info(brk_msg + "Calculating Aspect from {}...".format(smoothed_raster) + brk_msg)
            out_aspect = calc_aspect(smoothed_raster)
            out_aspect.save(os.path.join(output_directory_path, "aspect.tif"))

            # Calculate hillshade.
            logging.info(brk_msg + "Calculating Hillshade from {}...".format(smoothed_raster) + brk_msg)
            out_hillshade = calc_hillshade(smoothed_raster)
            out_hillshade.save(os.path.join(output_directory_path, "hillshade.tif"))

            # Calculate curvature (slope of slope) and save to disk.
            logging.info(brk_msg + "Calculating Curvature from {}...".format(smoothed_raster) + brk_msg)
            out_curvature = calc_curvature(smoothed_raster)
            out_curvature.save(os.path.join(output_directory_path, "curvature.tif"))

            logging.info(brk_msg)

            # Calculate and standardize Fine, Medium, and Broad BPI.
            f_bpi = calc_bpi(raster_layer, "fine", "3", "25", output_directory_path)
            standardize_bpi(f_bpi, f_bpi.replace("_temp", ""))
            logging.info(brk_msg)
            m_bpi = calc_bpi(raster_layer, "medium", "10", "100", output_directory_path)
            standardize_bpi(m_bpi, m_bpi.replace("_temp", ""))
            logging.info(brk_msg)
            b_bpi = calc_bpi(raster_layer, "broad", "25", "250", output_directory_path)
            standardize_bpi(b_bpi, b_bpi.replace("_temp", ""))

            # Calculate rugosity using Arc-Chord Ratio method.
            logging.info(brk_msg)
            calc_rug(smoothed_raster, output_directory_path)

        # If the user has specified to not smooth the bathymetry layer, use the input bathymetry to create
        # all derived layers
        else:
            # Calculate slope and save to disk.
            logging.info(brk_msg + "Calculating Slope from {}...".format(raster_layer) + brk_msg)
            out_slope = calc_slope(raster_layer)
            out_slope_path = os.path.join(output_directory_path, "slope.tif")
            out_slope.save(out_slope_path)

            # Calculate standard deviation of slope and save to disk.
            logging.info(brk_msg + "Calculating Standard Deviation of Slope from {}...".format(out_slope_path) + brk_msg)
            out_sd_slope = calc_sd_slope(out_slope_path, 3)

            # Calculate aspect.
            logging.info(brk_msg + "Calculating Aspect from {}...".format(raster_layer) + brk_msg)
            out_aspect = calc_aspect(raster_layer)
            out_aspect.save(os.path.join(output_directory_path, "aspect.tif"))

            # Calculate hillshade.
            logging.info(brk_msg + "Calculating Hillshade from {}...".format(raster_layer) + brk_msg)
            out_hillshade = calc_hillshade(raster_layer)
            out_hillshade.save(os.path.join(output_directory_path, "hillshade.tif"))

            # Calculate curvature (slope of slope) and save to disk.
            logging.info(brk_msg + "Calculating Curvature from {}...".format(raster_layer) + brk_msg)
            out_curvature = calc_curvature(raster_layer)
            out_curvature.save(os.path.join(output_directory_path, "curvature.tif"))

            logging.info(brk_msg)

            # Calculate and standardize Fine, Medium, and Broad BPI.
            f_bpi = calc_bpi(raster_layer, "fine", "3", "25", output_directory_path)
            standardize_bpi(f_bpi, f_bpi.replace("_temp", ""))
            logging.info(brk_msg)
            m_bpi = calc_bpi(raster_layer, "medium", "10", "100", output_directory_path)
            standardize_bpi(m_bpi, m_bpi.replace("_temp", ""))
            logging.info(brk_msg)
            b_bpi = calc_bpi(raster_layer, "broad", "25", "250", output_directory_path)
            standardize_bpi(b_bpi, b_bpi.replace("_temp", ""))

            # Calculate rugosity using Arc-Chord Ratio method.
            logging.info(brk_msg)
            calc_rug(Raster(raster_layer), output_directory_path)

    except Exception:
        logging.error(traceback.format_exc())


def set_extent(input_dir):
    """Gets the extents from raster layers. Creates the largest bounding box from coordinates.
    Sets extent for all layers to the new bounding box coordinates.

    :param input_dir: Input directory to get rasters from.
    :return:
    """

    try:
        # Set workspace with arcpy.
        arcpy.env.workspace = input_dir

        # The Statistics option enables you to build statistics for the output raster dataset.
        arcpy.env.rasterStatistics = "STATISTICS"

        # Set overwrite to True in environment.
        arcpy.env.overwriteOutput = True

        # Create list of raster layers in workspace.
        lyr_list = arcpy.ListRasters()

        # Create lists of coords.
        xmin = []
        ymin = []
        xmax = []
        ymax = []
        for layer in lyr_list:
            ext = arcpy.Describe(layer).extent
            xmin.append(ext.XMin)
            ymin.append(ext.YMin)
            xmax.append(ext.XMax)
            ymax.append(ext.YMax)

        # Get the coords for the largest bounding box from coords.
        xmin_coord = min(xmin)
        ymin_coord = min(ymin)
        xmax_coord = max(xmax)
        ymax_coord = max(ymax)

        bb_coords = {"xmin": xmin_coord, "ymin": ymin_coord, "xmax": xmax_coord, "ymax": ymax_coord}
        logging.info("Coordinates for largest bounding box: \n  XMIN: {}\n  YMIN: {}\n  XMAX: {}\n  YMAX: {}"
              .format(bb_coords["xmin"], bb_coords["ymin"], bb_coords["xmax"], bb_coords["ymax"]))

        # Set Arcpy extent in environment.
        arcpy.env.extent = "{} {} {} {}".format(bb_coords["xmin"],
                                                bb_coords["ymin"],
                                                bb_coords["xmax"],
                                                bb_coords["ymax"])

        for layer in lyr_list:
            ext = arcpy.Describe(layer).extent
            if ext.XMin == bb_coords["xmin"] and \
                    ext.YMin == bb_coords["ymin"] and \
                    ext.XMax == bb_coords["xmax"] and \
                    ext.YMax == bb_coords["ymax"]:
                logging.info("Extent for {} does not need to be changed.".format(layer))
                # Ignore this case because the layer has the same bounding box coordinates.
                continue
            logging.info("Extent for {} does not match largest bounding box. "
                  "Changing the extent and overwriting original.".format(layer))
            # Set extent of raster to match bounding box coords.
            ras = Raster(layer)
            # This does not change any values in the raster. Using the Math toolset from Spatial Analyst is
            # simplest way to set the extent of any rasters that need to be changed. Note: setting the overwriteOutput
            # option in the env settings has presented issues with writing the new rasters. Solution below is
            # basically to create a layer with a temp name, delete the original version and then rename the new layer.
            out_ras = Times(ras, 1.0)
            tmp_name = "temp.tif"
            out_ras.save(tmp_name)
            arcpy.Delete_management(layer)
            arcpy.Rename_management(tmp_name, layer)

    except Exception:
        logging.error(traceback.format_exc())
