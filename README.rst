Objective
=========

The purpose of these scripts is to generate a collection of raster surfaces derived from an input
bathymetry raster file using standardized methods.

Summary
=======
There is a python2 release (https://gitlab.com/dfo-msea/environmental-layers/bathy-derivatives/-/releases/v1.1) and a python3 release (https://gitlab.com/dfo-msea/environmental-layers/bathy-derivatives/-/releases/v2.0). 
See the python2 branch for specific README with instructions.

These scripts were written to support modelling exercises, where there was a need
for standardized environmental predictor layers including (but not exclusive to) bathymetry and its derivatives. The
derivatives that are calculated from bathymetry are:

* Arc-Chord Ratio Rugosity :sup:`1`
* Aspect
* Curvature :sup:`1`
* Hillshade
* Slope :sup:`1`
* Standard Deviation of Slope :sup:`1`
* Standardized Bathymetric Position Index at fine, medium, and broad scales

:sup:`1` If option to smooth bathymetry is selected, these layers derived from smoothed version.

Additional layers exported from the script may include:

* Bathymetry Flipped: the copied bathymetry layer with a sign change so that depth values are negative.
* Bathymetry Smoothed: the copied bathymetry layer smoothed by a user-defined number of cells.

Execute run.py with a python virtual environment that has access to ArcPro Python (Python3).

Code must be run with Python3 environment with access to the arcpy module as well as the other required modules (see Requirements). 
Follow the guide here: https://support.esri.com/en/technical-article/000020560 to copy the ArcPro python virtual environment that uses Python3 so that you 
can install other required modules.

Example (assuming your new conda env is called 'arcpropy'): 

**Activate virtual env**

`conda activate arcpropy` 

**Change to directory with scripts** 

``D:``

``cd projects\bathymetry\code\bathy-derivatives`` 


**Execute program** 

``python run.py``

The script will prompt the user for input.

A messages.log file will be written to the output directory. This does not contain the messages printed to the console
during execution. It will contain the date the script was executed and any details for changing positive values to
negative in the bathymetry and smoothing the input bathymetry.

Status
======
Ongoing-improvements.

Contents
========
**run.py**: The main script that should be executed by the user. This calls a series of functions using the ArcPy
module scripts from the Bathymetric Terrain Modeler Toolbox scripts.

**env_data_functions**: Main supporting script with functions to create derivatives. Some functions use
functions from the supporting modules such as to generate the BPI layers.

**bpi, config, utils, surface_area_to_planar_area**: Supporting modules from the BTM toolbox which have been slightly
modified.

Methods
=======
The script executes a series of ArcPy and BTM functions to create derivatives from the input bathymetry.
Slope is calculated in degrees. Rugosity is calculated using the Arc-Chord Ratio method.
Bathymetric Position Index (BPI) layers are calculated at 3 scales (broad, medium, and fine) to capture features
at the local scale as well as larger features identifiable with a larger neighbourhood of cells.

There is an option to smooth the input bathymetry prior to creating some of the layers (slope, standard deviation of
slope, curvature, and rugosity). The reason for (optionally) smoothing the bathymetry is to reduce artefacts in the
derivatives where the bathymetry may be poorly interpolated resulting in gridded patterns in the output.

The output layers are also aligned by setting the snap raster environment with ArcPy and the extents are checked to
ensure that all layers have the same extent. To do this, a bounding box is created from the minimum xmin and ymin
coordinates and the maximum xmax and ymax coordinates. If any layers do not have the extent of the bounding box, then a
copy with the correct extent is created to replace the original. Messages are sent to a log file once the
geoprocessing has started.

Requirements
============
* os
* arcpy
* arcpy.sa import *
* surface_area_to_planar_area as acr
* bpi
* utils
* logging
* rasterio
* numpy
* scipy 
* traceback
* Other scripts in the repository (including several from the BTM toolbox).

Caveats
=======
* Once the output directory has been created by the script, messages normally printed to the console will be
  redirected to the log file.

* The functions in the BTM scripts expect negative values for the input bathymetry dataset.

Uncertainty
===========
* There is not really a formula for determining if smoothing of the bathymetry is necessary or how much smoothing
  should be done prior to calculating the derivatives.

* Generating standard deviation of slope has sometimes produced unexpected results where there are gaps in the output surface. 
This may be due to memory issues with large rasters. Advise looking at the derivatives to determine if there are any errors.

Acknowledgements
================
Jessica Nephin: DFO Science, Marine Spatial Ecology and Analysis section.

* https://gitlab.com/jnephin/bathy-complexity/-/blob/master/complexity_example.ipynb

References
==========
* Walbridge, S.; Slocum, N.; Pobuda, M.; Wright, D.J. Unified Geomorphological Analysis Workflows with Benthic
  Terrain Modeler. Geosciences 2018, 8, 94. doi:10.3390/geosciences8030094
* Du Preez, C. A new arc–chord ratio (ACR) rugosity index for quantifying three-dimensional landscape structural
  complexity. Landscape Ecol 30, 181–192 (2015). https://doi.org/10.1007/s10980-014-0118-8
* https://www.arcgis.com/home/item.html?id=b0d0be66fd33440d97e8c83d220e7926
* https://coast.noaa.gov/data/digitalcoast/pdf/btm-tutorial.pdf
